import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.channel.voice.VoiceChannelDeleteEvent;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.guild.voice.GenericGuildVoiceEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.exceptions.PermissionException;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.utils.SimpleLog;
import org.json.simple.parser.ParseException;

import javax.security.auth.login.LoginException;
import java.io.*;
import java.time.Instant;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Xavier on 4/1/2017.
 */


public class Xonah extends ListenerAdapter {
    private static JDA jda; //Declare the JDA object in this scope, for easier access instead of accessing it through events.
    private static Instant date; //Last active date for Xonah
    private static List<String> moduleNames = Arrays.asList("Tags", "Dynamic Voice Rooms", "Logging", "Custom Role Registration", "KickBanReasons");

    public static void main(String[] args) throws LoginException, RateLimitedException, IOException, InterruptedException, ParseException {
        new Xonah();
    }

    private Xonah() throws LoginException, RateLimitedException, IOException, InterruptedException, ParseException {
        String token = init(); //Fetch properties, make sure files/folders exist.
        SimpleLog.getLog("Init").info("Initializing with token: " + token);

        jda = new JDABuilder(AccountType.BOT).setToken(token)
                .addEventListener(this).buildAsync(); //Start the bot, store it in our earlier declared JDA object.

        while (!(jda.getStatus().equals(JDA.Status.CONNECTED))) {Thread.sleep(10);} //Wait until connection is established.

        SimpleLog.getLog("Post-Init").info("Beginning verification that couldn't be done without API calls.");
        //Checks on JSON objects, SQL tables, etc. Return a list of guilds that had errors in the JSON file.
        postInit();
        jda.getPresence().setGame(Game.of("Commands have changed! Type ~help!"));
    }

    private String init() throws IOException {
        String token;
        SimpleLog.getLog("Init").info("Retrieving properties and reading JSON files...");
        //Open a file reader to the properties file, then load it into a properties object and fetch the token, write it to string.\
        FileReader reader = new FileReader(new File("config.properties"));

        Properties props = new Properties();
        props.load(reader);

        token = props.getProperty("token");
        if(!props.containsKey("date")) {props.setProperty("date", String.valueOf(new Date().getTime()));} else {
            date = new Date(Long.valueOf(props.getProperty("date"))).toInstant();
        }
        props.store(new FileOutputStream(new File("config.properties")), "");
        reader.close();

        SimpleLog.getLog("Init").info("Retrieved token OK");
        SimpleLog.getLog("Init").info("Checking SQLite database integrity (Stage 1 of 2)...");

        //Declare the director for, and the the individual database files, check for their existence, create them if not there.
        File dir = new File('.' + File.separator + "databases");
        File[] dbs = new File[]
                {new File(dir,"tags.db"), new File(dir, "messagecache.db"), new File(dir, "DVRTrack.db")};

        if(!dir.exists()) {SimpleLog.getLog("Init").warn("Root database director missing!"); dir.mkdir();}
        for(int i=0;i!=dbs.length;i++) {
            if(!dbs[i].exists()){SimpleLog.getLog("Init").warn(dbs[i].getName() + " is missing!"); dbs[i].createNewFile();}
        }

        SimpleLog.getLog("Init").info("Finished stage 1 of 2 of SQLite database integrity check.");
        SimpleLog.getLog("Init").info("Checking JSON configuration files(Stage 1 of 3)...");

        //Do the same thing for JSON
        File jsonConfig = new File("guildconfig.json");

        if(!jsonConfig.exists()) {
            SimpleLog.getLog("Init").warn("Json file missing!");
            jsonConfig.createNewFile();
            FileWriter writer = new FileWriter(jsonConfig);
            writer.write("{\n\n}");
            writer.close();
        }

        SimpleLog.getLog("Init").info("Finished stage 1 of 2 of JSON configuration check.");
        SimpleLog.getLog("Init").info("Initialization completed successfully!");

        return token;
    }

    private void postInit() throws IOException, ParseException {
        SimpleLog.getLog("Checking JSON configuration files(Stage 2 of 3)...");
        new ModuleCore(jda.getGuilds()).init();
        SimpleLog.getLog("Post-Init").info("JSON configuration verified, and all changes written to file.");
        SimpleLog.getLog("Post-Init").info("Checking SQLite database files(Stage 2 of 2)...");
        new SQLiteCore(jda.getGuilds()).dbChecks(); //Run through basic table checking in the SQLiteCore.
        SimpleLog.getLog("Post-Init").info("SQLite databases verified, all missing tables replaced.");
        SimpleLog.getLog("Post-Init").info("Inputting messages missed while offline...");
        if(date != null) {new SQLiteCore(jda.getGuilds()).populateLog(date);}
        SimpleLog.getLog("Post-Init").info("Post initialization completed!");
    }

    /*
     * Command structure/Event listener structure below.
     *
     * In here there are actually 2 command structures, one is specific to guild owners for management of the role system.
     * The other is general commands exposed to everybody else, though it does callback to ModuleCore to verify
     * that those commands are supposed to run on a given guild.
     *
     * The logging functions are also stored here as well, and will make similar calls to ModuleCore
    */


    //This is just an easy way to mark down the last time Xonah was active, which is used in startup, should Xonah go down.
    @Override
    public void onGenericEvent(Event event) {
        try {
            FileReader reader = new FileReader(new File("config.properties"));
            Properties props = new Properties();
            props.load(reader);

            props.setProperty("date", String.valueOf(new Date().getTime()));

            props.clone();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            SimpleLog.getLog("Logging").warn("Failed logging time!");
        }
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        new ModuleCore(event.getGuild(), true);
        new SQLiteCore(event.getGuild(), true);
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        SimpleLog.getLog("Guilds").info("Left guild " + event.getGuild().getName() + "! Deleting related info...");
        new ModuleCore(event.getGuild(), true);
    }

    @Override
    public void onVoiceChannelDelete(VoiceChannelDeleteEvent event) {
        new SQLiteCore(event.getGuild(), false).deleteRoom(event.getChannel().getId());
    }

    @Override
    public void onGenericGuildVoice(GenericGuildVoiceEvent event) {
        List<VoiceChannel> channels = new ArrayList<>();

        try {
            if(event.getMember().getVoiceState().inVoiceChannel()) {
                new SQLiteCore(event.getGuild(), false).enableTracking(event.getMember().getVoiceState().getChannel().getId());
            }

            for(String id : new SQLiteCore(event.getGuild(), false).listTrackedRooms()) {
                channels.add(event.getGuild().getVoiceChannelById(id));
            }
            for(VoiceChannel channel : channels) {
                if(channel.getMembers().isEmpty()){channel.delete().queue();}
            }
        } catch (ErrorResponseException e) {}

    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        if(new ModuleCore(event.getGuild(), false).getModules().contains("Logging")) {
            TextChannel channel = event.getGuild().getTextChannelById(new ModuleCore(event.getGuild(), false).getLChannelID());

            channel.sendMessage(new EmbedBuilder().setDescription("**MESSAGE EDITED**")
                    .addField("User", event.getAuthor().getName(), true)
                    .addField("Channel", event.getChannel().getName(), true)
                    .addField("Original Message", new SQLiteCore(event.getGuild(), false).cacheGet(event.getMessageId())
                        .get(0), false)
                    .addField("Current Message", event.getMessage().getContent(), false)
                    .setFooter(String.valueOf(new Date()), "http://i.imgur.com/HgtQmeZ.jpg")
                    .build()).queue();
        }
    }

    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        if(new ModuleCore(event.getGuild(), false).getModules().contains("Logging")) {
            List<String> message = new SQLiteCore(event.getGuild(), false).cacheGet(event.getMessageId());
            TextChannel channel = event.getGuild().getTextChannelById(
                    new ModuleCore(event.getGuild(), false).getLChannelID());
            User user = jda.getUserById(message.get(1));
            channel.sendMessage(new EmbedBuilder().setDescription("**MESSAGE DELETED**")
                    .addField("User", user.getName(), true)
                    .addField("Channel", event.getChannel().getName(), true)
                    .addField("Message", message.get(0), false)
                    .setFooter(String.valueOf(new Date()), "http://i.imgur.com/HgtQmeZ.jpg")
                    .build()).queue();
        }
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event){
        String message = event.getMessage().getContent();

        if(event.getMessage().getContent().isEmpty()) {
            new SQLiteCore(event.getGuild(), false).cacheStore("Embedded images not supported yet! :no_entry_sign:", event.getMessageId(), event.getAuthor().getId());
        } else {
            new SQLiteCore(event.getGuild(), false).cacheStore(event.getMessage().getContent(), event.getMessageId(), event.getAuthor().getId());


            try {
                if (!event.getAuthor().equals(event.getJDA().getSelfUser()) && event.getMessage().getContent().charAt(0) == '~'
                        && !event.getMessage().isWebhookMessage()) {
                    router(arguments(message.substring(1, message.length())), event);
                } else if(event.getMessage().getContent().charAt(0) == '-') {
                    event.getChannel().sendMessage("Please use a tilde, not a dash! :heart:").queue();
                }
            } catch (PermissionException e) {
                event.getGuild().getOwner().getUser().openPrivateChannel().complete()
                        .sendMessage("Xonah is missing permissions to do a command!\n" +
                                "Permission: " + e.getPermission().getName()).queue();
            }
        }
    }

    /*
        Main router for all commands, has conditional switch cases only used if a given module is turned on
        or a given user has permission to use a command.
     */

    private void router(List<String> args, GuildMessageReceivedEvent event) throws PermissionException {
        List<String> availableModules = new ModuleCore(event.getGuild(), false).getModules();

        //Root command routing
        switch (args.get(0)) {
            //Base commands with no subcommands
            case "help":
                event.getAuthor().openPrivateChannel().complete()
                        .sendMessage(new TCommands().help(event.getMember())).queue();
                break;

            case "ping":
                event.getChannel().sendMessage("Pong!").queue();
                break;

            case "feedback ":
                User laeron = jda.getUserById("126551534447099905");
                laeron.openPrivateChannel().complete()
                        .sendMessage(event.getAuthor().getName() + " from " + event.getGuild().getName() +
                                " says\n" + args).queue();
                break;

            case "info":
                event.getAuthor().openPrivateChannel().complete()
                        .sendMessage(new TCommands().info(event.getAuthor(), jda)).queue();
                break;

            //Dynamic Voice Room subcommand structure
            case "dvr":
                if(availableModules.contains("Dynamic Voice Rooms")) {
                    switch (args.get(1)) {
                        case "newRoom":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage(new VCommands().room(args.get(2), event.getAuthor().getId(),
                                            event.getGuild().getController())).queue();
                            break;

                        case "setName":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage(new VCommands().setDVRName(args.get(2), event.getAuthor().getId(),
                                            event.getGuild().getController())).queue();
                            break;

                        case "userLimit":
                            try {
                                event.getAuthor().openPrivateChannel().complete()
                                        .sendMessage(new VCommands().setUserLimit(Integer.valueOf(args.get(2)), event.getAuthor().getId(),
                                                event.getGuild().getController())).queue();
                            } catch (NumberFormatException e) {
                                event.getAuthor().openPrivateChannel().complete()
                                        .sendMessage("This value must be a whole number!").queue();
                            }
                            break;
                    }
                }
                break;

            //Tag system subcommand structure
            case "tags":
                if(availableModules.contains("Tags")) {
                    switch (args.get(1)) {
                        case "tag":
                            event.getChannel().sendMessage(new TCommands().tag(args.get(2), event.getGuild())).queue();
                            break;

                        case "info":
                            event.getChannel().sendMessage(new TCommands().tagi(args.get(2), event.getGuild())).queue();
                            break;

                        case "list":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage((new TCommands().tagl(event.getGuild()))).queue();
                            break;

                        case "create":
                            if(event.getMessage().getAttachments().isEmpty()) {
                                event.getChannel().sendMessage(new TCommands().tagc(args.get(2), event.getGuild(),
                                        event.getAuthor().getName(), event.getAuthor().getId())).queue();
                            } else {
                                event.getChannel().sendMessage("Uploaded images cannot be tags! \n" +
                                        "Please use an Imgur link or some other image host!\n" +
                                        "Preferably something Discord embeds.").queue();
                            }
                            break;

                        case "delete":
                            if(new ModuleCore(event.getGuild(), false).getBotmasters().contains(event.getAuthor().getId())
                                    || event.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                                event.getChannel().sendMessage(new TCommands().tagd(args.get(2), event.getGuild())).queue();
                            }
                            break;
                    }
                }
                break;

            //Custom Role Registration subcommand structure
            case "roles":
                if(availableModules.contains("Custom Role Registration")) {
                    switch (args.get(1)) {
                        case "register":
                            try {
                                if (event.getChannel().getId().equals(new ModuleCore(event.getGuild(), false).getRChannelID())) {
                                    try {
                                        Message message = event.getChannel().sendMessage(new TCommands()
                                                .register(event.getMember(), event.getGuild(), args.get(2))).complete();
                                        Thread.sleep(1000);
                                        event.getChannel().deleteMessageById(message.getId()).queue();
                                        Thread.sleep(10);
                                        event.getChannel().deleteMessageById(event.getMessage().getId()).queue();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                        SimpleLog.getLog("Role Registration").warn("Error occured deleting messages after" +
                                                "registration!");
                                    }
                                    } else if (new ModuleCore(event.getGuild(), false).getRChannelID().equals("ERROR")) {
                                        event.getGuild().getOwner().getUser().openPrivateChannel().complete()
                                                .sendMessage("Custom role registration is on, " +
                                                        "but no registration channel set!\n" +
                                                        "Please use ~setRegisterChannel and mention a channel to set one!");
                                    } else {
                                        event.getChannel().sendMessage(new MessageBuilder().append("Please use ")
                                                .append(event.getGuild().getTextChannelById(
                                                        new ModuleCore(event.getGuild(), false).getRChannelID()).getAsMention())
                                                .append(" for registering roles! :heart:")
                                                .build()).queue();
                                    }
                                } catch (PermissionException e) {
                                    event.getGuild().getOwner().getUser().openPrivateChannel().complete()
                                            .sendMessage("Xonah is missing permissions to manage roles or delete messages!").queue();
                                }
                                break;

                        case "list":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage(new TCommands().rolel(event.getGuild())).queue();
                            break;

                        case "add":
                            if (new ModuleCore(event.getGuild(), false).getBotmasters().contains(event.getAuthor().getId())
                                    || event.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                                if (!event.getMessage().getMentionedRoles().isEmpty()) {
                                    event.getAuthor().openPrivateChannel().complete()
                                            .sendMessage(new TCommands()
                                                    .role(event.getMessage().getMentionedRoles().get(0).getId(),
                                                            event.getGuild())).queue();
                                } else {
                                    event.getAuthor().openPrivateChannel().complete()
                                            .sendMessage(new TCommands().role(event.getGuild().getRolesByName(args.get(2), false).get(0)
                                                    .getId(), event.getGuild())).queue();
                                }
                            }
                            break;

                        case "setChannel":
                            if (new ModuleCore(event.getGuild(), false).getBotmasters().contains(event.getAuthor().getId())
                                    || event.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                                if (event.getMessage().getMentionedChannels().isEmpty()) {
                                    event.getChannel().sendMessage("No channel listed!").queue();
                                } else {
                                    new TCommands().setRChannel(event.getMessage().getMentionedChannels().get(0).getId(), event.getGuild());
                                    event.getAuthor().openPrivateChannel().complete()
                                            .sendMessage("Channel added!").queue();
                                }
                            }
                            break;

                        default:
                            break;
                        }
                    }
                break;

            //Module core subcommand structure
            case "module":
                if(args.get(1).equals("list")){
                    event.getAuthor().openPrivateChannel().complete()
                            .sendMessage(new TCommands().moduleL(event.getGuild(), moduleNames)).queue();
                } else if(event.getMember().hasPermission(Permission.MANAGE_SERVER) || event.getAuthor().getId().equals("126551534447099905")) {
                    switch (args.get(1)) {
                        case "toggle":
                            if(event.getMessage().getContent().contains("logging") && new ModuleCore(event.getGuild(), false).getLChannelID().equals("ERROR")) {
                                event.getAuthor().openPrivateChannel().complete()
                                        .sendMessage("**WARNING:** Set a logging channel before enabling logging.").queue();
                            } else {
                                event.getAuthor().openPrivateChannel().complete()
                                        .sendMessage(new TCommands().toggle(event.getGuild(), moduleNames, args.get(2))).queue();
                            }
                            break;

                        case "masters":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage(new TCommands().masters(event.getGuild())).queue();
                            break;

                        case "master":
                            event.getAuthor().openPrivateChannel().complete()
                                    .sendMessage(new TCommands()
                                            .master(event.getMessage().getMentionedUsers().get(0), event.getGuild())).queue();
                            break;

                        case "setLoggingChannel":
                            if(event.getMessage().getMentionedChannels().isEmpty()) {
                                event.getChannel().sendMessage("No channel listed!").queue();
                            } else {
                                new TCommands().setLChannel(event.getMessage().getMentionedChannels().get(0).getId(), event.getGuild());
                                event.getAuthor().openPrivateChannel().complete()
                                        .sendMessage("Channel added!").queue();
                            }
                            break;

                        default:
                            //Do nothing
                            break;
                    }
                }
                break;

            default:
                //Do nothing
                break;
        }

        //Global botmaster commands
        switch (args.get(0)) {
            case "quit":
                if(event.getAuthor().getId().equals("126551534447099905")) {
                    jda.shutdown();
                }
                break;
        }
    }

    private List<String> arguments(String message) {
        int length = 0; //Keeps track of how far down the message we've been.
        boolean stop = false; //Flag to stop scanning
        List<String> args = new ArrayList<>();
        do {
            length++;
            if(length >= message.length()) {stop = true;}
            else if(Character.isWhitespace(message.charAt(length))) {stop = true;}
        } while (!stop);
        stop = false; //Reset the flag
        args.add(message.substring(0, length).toLowerCase()); //Add the argument to the list, lowercase for processing
        if(Stream.of("ping", "info", "help", "feedback", "quit").noneMatch(x -> args.get(0).equals(x))) {
            message = message.substring(length, message.length());
            length = 0;
            do {
                length++;
                if(length >= message.length()) {stop = true;}
                else if(Character.isWhitespace(message.charAt(length))) {stop = true;}
            } while (!stop);
            args.add(message.substring(1, length));
            if(length != message.length()) {args.add(message.substring(length + 1, message.length()));}
        }
        return args;
    }
}