import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.exceptions.PermissionException;

import java.util.List;

/**
 * Created by Xavier on 1/17/2017.
 */
class TCommands
{
    //Public commands that are available in Xonah by default.

    MessageEmbed help(Member user){
        List<String> modules = new ModuleCore(user.getGuild(), false).getModules();
        EmbedBuilder embedBuilder = new EmbedBuilder() //instantiate the builder, add the default commands.
                .setTitle("List of commands in " + user.getGuild().getName(), null)
                .setDescription("These are the commands available in this guild!\n"
                        + "Format: () = Required {} = optional\n"
                        + "Example: `~tags create example hello world!`\n"
                        + "**NOTE:** These brackets are not required when executing a command!")
                .addField("~ping", "Pong!", true)
                .addField("~help", "You're seeing it now!", true)
                .addField("~feedback (feedback)", "Report bugs or offer ideas!", true)
                .addField("~info", "Shows info on Xonah", false)
                .addField("~module list", "Lists off modules and if they're enabled.", true);

        if(modules.contains("Tags")) {
            embedBuilder
                    .addBlankField(false)
                    .addField("Tags", "Creates tags that can be called up in the future!\n" +
                            "Prefix is 'tags'.", false)
                    .addField("create (tagname) (tagcontents)", "Creates a new tag", true)
                    .addField("list", "DMs you a list of tags in this server.", true)
                    .addField("tag (tagname)", "References the tag by this name.", true)
                    .addField("delete (tagname)", "Deletes a tag. **BOTMASTER ONLY!**", true)
                    .addField("info (tagname)", "Gets info on a tag.", false);
        }

        if(modules.contains("Dynamic Voice Rooms")) {
            embedBuilder
                    .addBlankField(false)
                    .addField("Dynamic Voice Rooms", "These are voice rooms that can be created by users on demand.\n" +
                            "Prefix is `dvr`.", false)
                    .addField("newRoom (name)", "Creates a new voice room.", true)
                    .addField("setName (room name)", "Changes the name of the voice room.", true)
                    .addField("userLimit (number **WHOLE NUMBER ONLY**)", "Sets the user limit of the voice room.", false);
        }

        if(modules.contains("Custom Role Registration")) {
            embedBuilder
                    .addBlankField(false)
                    .addField("Custom Role Registration", "Role assignment without requiring an admin!\n" +
                            "Prefix is 'roles'.", false)
                    .addField("register (role)", "Assign yourself a role!", true)
                    .addField("list", "List assignable roles!", true)
                    .addField("toggleRole (role) OR (mentionrole)", "Add/Remove an assignable role! **BOTMASTER ONLY!**", true)
                    .addField("setChannel (mentionchannel)", "Set the register channel! **BOTMASTER ONLY!**", false);
        }

        /*if(modules.contains("KickBanReasons") && new ModuleCore(user.getGuild(), false).getBotmasters().contains(user.getUser().getId())) {
            embedBuilder
                    .addField("Kick/Ban Reasons", "Explaining bans and kicks!", false)
                    .addField("~setKickBanChannel (mentionchannel)", "Set the channel bans/kicks will be recorded in.", true)
                    .addField("~reason (eventnumber) (reason)", "Give the kick/ban number, and give a reason.", true)
                    .addField("~shadow", "Ignores the next kick/ban.", false);
        }*/

        if(user.hasPermission(Permission.MANAGE_SERVER) || user.getUser().getId().equals("126551534447099905")) {
            embedBuilder
                    .addBlankField(false)
                    .addField("Xonah Setup", "Commands for setting up Xonah in this server.\n" +
                            "Prefix is `module`.", false)
                    .addField("toggle (module)", "Toggles the provided module.", true)
                    .addField("masters", "Lists off bot masters in this server.", true)
                    .addField("master (usermention)", "Adds mentioned user as a botmaster.\n"
                            + "If they are already a master, they'll be removed.", false)
                    .addField("setLoggingChannel (mentionchannel)", "Set the logging channel!", false);
        }

        return embedBuilder.addBlankField(false)
                .setFooter("Some commands may not be listed, currently " + modules.size() + "/5" +
                " modules enabled!", null).build();
    }

    MessageEmbed info(User user, JDA jda) {
        return new EmbedBuilder()
                .setTitle("Xonah information", null)
                .appendDescription("Version, BitBucket page, etc.")
                .addField("BitBucket", "https://bitbucket.org/XavierCline/xonah", true)
                .addField("Version", "0.1.1", true)
                .addField("Current Servers", String.valueOf(jda.getGuilds().size()), true)
                .addField("Number of mutual servers", String.valueOf(jda.getMutualGuilds(user).size()), false)
                .setFooter("Created by Xavier (Laeron) Cline", "http://i.imgur.com/HgtQmeZ.jpg").build();
    }

    /*
        Tag System below.
     */

    String tagc(String arg, Guild guild, String user, String userid) {
        char currentChar = 'a';
        int nameEnd;
        try {
            for(nameEnd=0;!(Character.isWhitespace(currentChar));nameEnd++) {
                currentChar = arg.charAt(nameEnd);
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return "Something went wrong parsing the tag! Contact Laeron is this persists!";
        }
        return new SQLiteCore(guild, false).tagc(arg.substring(0,nameEnd-1), arg.substring(nameEnd), user, userid);
    }

    String tagd(String tag, Guild guild) {return new SQLiteCore(guild, false).tagd(tag);}

    String tag(String tag, Guild guild) {return new SQLiteCore(guild, false).tag(tag);}

    MessageEmbed tagl(Guild guild) {return new SQLiteCore(guild, false).tagl();}

    MessageEmbed tagi(String tag, Guild guild) {
        return new SQLiteCore(guild, false).tagi(tag);
    }

    /*
        Custom Role Registration system below
     */

    String register(Member user, Guild guild, String role) throws PermissionException {
        String returnStr;
        String roleID = guild.getRolesByName(role, true).get(0).getId();

        if(new ModuleCore(guild, false).getRoles().contains(roleID)) {
            guild.getController().addRolesToMember(user, guild.getRoleById(roleID)).queue();
            returnStr = "Role assigned!";
        } else {returnStr = "This is not a role! Is it typed correctly?";}

        return returnStr;
    }

    MessageEmbed rolel(Guild guild) {
        EmbedBuilder returnMsg = new EmbedBuilder().setTitle("Current roles in " +
                guild.getName(), null);

        if(!new ModuleCore(guild, false).getRoles().isEmpty()) {
            List<String> roles = new ModuleCore(guild, false).getRoles();
            returnMsg.appendDescription("List of current roles able to be registered.");

            for(String role : roles) {
                returnMsg.addField(guild.getRoleById(role).getName(), role, true);
            }
        } else {
            returnMsg.appendDescription("No assignable roles have been setup!");
        }

        return returnMsg.build();
    }

    String role(String roleID, Guild guild) {
        new ModuleCore(guild, false).setRole(roleID);
        String returnStr = (new ModuleCore(guild, false).getRoles().contains(roleID)) ?
                "added to " : "removed from ";
        return guild.getRoleById(roleID).getName() + " has been " + returnStr + "the role list!";
    }

    void setRChannel(String channelID, Guild guild) {
        new ModuleCore(guild, false).setRChannel(channelID);
    }

    /*
        Module Configuration system below.
     */

    MessageEmbed moduleL(Guild guild, List<String> moduleNames) {
        List<String> modules = new ModuleCore(guild, false).getModules();
        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setTitle("List of activated modules in Xonah", null);

        for(String name : moduleNames) {
            embedBuilder.addField(name,
                    (modules.contains(name)) ? "Enabled": "Disabled", true);
        }

        return embedBuilder.build();
    }

    String toggle(Guild guild, List<String> moduleNames, String module) {
        String returnStr = "Module " + module + " does not exist! Is there a typo?";
        if(moduleNames.contains(module)) {
            new ModuleCore(guild, false).toggleModule(module);
            returnStr = (new ModuleCore(guild, false).getModules().contains(module))
                    ? module + " enabled!" : module + " disabled!";
        }
        return returnStr;
    }

    MessageEmbed masters(Guild guild) {
        EmbedBuilder returnMsg = new EmbedBuilder().setTitle("Current botmasters in " + guild.getName(), null);

        if(!new ModuleCore(guild, false).getBotmasters().isEmpty()) {
            List<String> masterIDs = new ModuleCore(guild, false).getBotmasters();
            returnMsg.appendDescription("List of current bot masters.");

            for(String id : masterIDs) {
                returnMsg.addField(guild.getMemberById(id).getEffectiveName() + "", id + "", true);
            }
        } else {
            returnMsg.appendDescription("No botmasters found! Use `~master` and mention a user in your server" +
                    " to add a new botmaster!");
        }

        return returnMsg.build();
    }

    String master(User master, Guild guild) {
        new ModuleCore(guild, false).setBotmaster(master.getId());
        String returnStr = (new ModuleCore(guild, false).getBotmasters().contains(master.getId())) ?
                "added " : "removed ";
        return master.getName() + " has been " + returnStr + "from the botmaster list!";
    }

    void setLChannel(String channelID, Guild guild) {
        new ModuleCore(guild, false).setLChannel(channelID);
    }
}
