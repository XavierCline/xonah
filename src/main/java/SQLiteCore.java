import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.utils.SimpleLog;

import java.io.File;
import java.sql.*;
import java.time.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xavie on 5/15/2017.
 */
class SQLiteCore {
    //Variables for maintaining the connection to the database and statements.
    //These are listed as null as to make sure there is no left over junk data.
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement pStatement = null;

    //Variable for flagging if we're in init mode.
    private boolean isInit;

    //Other variables commonly used across functions that get stored in the constructor.
    private Guild guild;
    private List<Guild> guilds;

    //Constructors for use on initialization, and for regular use.
    SQLiteCore(List<Guild> initGuilds) { //Only used in initialization.
        guilds = initGuilds;
        isInit = true;
    }

    //For normal non-init purposes. The joining bool defines what path we're going on.
    SQLiteCore(Guild callGuild, boolean joining) { //For normal use
        if(joining) {
            String[] dbs = new String[]
                    {"tags", "messagecache", "DVRTrack"};
            try {
                for(String db : dbs) {
                    openConnection(db);
                    switch (db) {
                        case "tags":
                            statement.execute("create table if not exists [" + callGuild.getId() + "] " +
                                    "(uid integer constraint constraint_name primary key, name text, tag text, user text, " +
                                    "userid text, date numeric)");
                            break;

                        case "messageCache":
                            statement.execute("create table if not exists [" + callGuild.getId() + "] " +
                                    "(id text, userid text, date integer, message text)");
                            break;

                        case "DVRTrack":
                            statement.execute("create table if not exists [" + callGuild.getId() + "] " +
                                    "(id text, userid text, tracking integer)");
                            break;

                        default:
                            break;
                    }
                    closeConnection();
                }
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                closeConnection();
            }
        }
        guild = callGuild;
        isInit = false;
    }

    //Only runs when the init constructor is used.
    //Checks to make sure all tables exist in all databases.
    void dbChecks() {
        if(isInit) {
            try {
                openConnection("tags");
                for(Guild guild : guilds) {
                    statement.execute(
                            "create table if not exists [" + guild.getId() + "] (uid integer constraint constraint_name primary key, "
                                    + "name text, tag text, user text, userid text, date numeric)"
                    );
                }
                closeConnection();
                openConnection("messageCache");
                for(Guild guild : guilds) {
                    statement.execute("create table if not exists [" + guild.getId() + "] " +
                            "(id text, userid text, date integer, message text)");
                }
                closeConnection();
                openConnection("DVRTrack");
                for(Guild guild : guilds) {
                    statement.execute("create table if not exists [" + guild.getId() + "] " +
                            "(id text, userid text, tracking integer)");
                }
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
                SimpleLog.getLog("SQLite").warn("Error in post initialization!");
            } finally {
                closeConnection();
            }
        } else {
            SimpleLog.getLog("SQLiteCore").warn("Attempting to access dbChecks outside of init mode!");
        }
    }

    //Only runs when init constructor is used
    //Repopulates the message cache to provide near seamless operation of the logging mechanic through shutdowns
    void populateLog(Instant lastOnline) {
        for(Guild guild : guilds) {
            if(new ModuleCore(guild, false).getModules().contains("logging")) {
                for(TextChannel channel : guild.getTextChannels()) {
                    List<Message> messages = channel.getHistory().retrievePast(100).complete();
                    for(Message message : messages) {
                        if (message.getCreationTime().isAfter(ZonedDateTime.ofInstant(lastOnline, ZoneId.systemDefault()).toOffsetDateTime())) {
                            cacheStore(message.getContent(), message.getId(), message.getAuthor().getId(), guild.getId());
                        }
                    }
                }
            }
        }
    }

    /*
        Cache system implemented below.
     */
    void cacheStore(String message, String messageID, String userID) {
        try {
            openConnection("messageCache");
            ResultSet rs = statement.executeQuery("select date from [" + guild.getId() + "]");
            if (!rs.next()) {
                SimpleLog.getLog("SQLite").info("MessageCache DB empty!");
            } else {
                statement.executeUpdate("delete from [" + guild.getId() + "] where date < '" + new java.util.Date(System.currentTimeMillis() - 3600 * 2000).getTime() +"'");
            }
            pStatement = connection.prepareStatement("insert into [" + guild.getId() + "] (id, date, message, userid) values (?," +
                    new java.util.Date().getTime() + ",?, ?)");
            pStatement.setString(1,messageID);
            pStatement.setString(2, message);
            pStatement.setString(3, userID);
            pStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    private void cacheStore(String message, String messageID, String userID, String guildID) {
        try {
            openConnection("messageCache");
            ResultSet rs = statement.executeQuery("select date from [" + guildID + "]");
            if (!rs.next()) {
                SimpleLog.getLog("SQLite").info("MessageCache DB empty!");
            } else {
                statement.executeUpdate("delete from [" + guildID + "] where date < '" + new java.util.Date(System.currentTimeMillis() - 3600 * 2000).getTime() +"'");
            }
            pStatement = connection.prepareStatement("insert into [" + guildID + "] (id, date, message, userid) values (?," +
                    new java.util.Date().getTime() + ",?, ?)");
            pStatement.setString(1,messageID);
            pStatement.setString(2, message);
            pStatement.setString(3, userID);
            pStatement.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    List<String> cacheGet(String messageID) {
        //Position 0 is always the message, postion 1 is always the userID
        List<String> returnMessage = new ArrayList<>();
        try {
            openConnection("messageCache");
            ResultSet rs = statement.executeQuery("select * from [" + guild.getId() + "] where id == '" + messageID + "'");
            if(!rs.next()) {
                SimpleLog.getLog("SQLite").warn("No such id found! Is the prune time too short?");
            } else if (rs.getFetchSize() != 0) {
                SimpleLog.getLog("SQLite").warn("Multiple instances of this id! This should never happen!");
            } else {
                do {
                    returnMessage.add(rs.getString("message"));
                    returnMessage.add(rs.getString("userid"));
                } while (rs.next());
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return returnMessage;
    }

    /*
        Tag system implemented below.
     */
    String tagc(String name, String tag, String user, String userID) {
        String returnString = "Failed to create tag. If this error persists, contact Laeron!";
        try {
            openConnection("tags");
            pStatement = connection.prepareStatement("select name from [" + guild.getId() + "] where name = ?");
            pStatement.setString(1,name);
            ResultSet rs = pStatement.executeQuery();
            if(rs.next()) {return "This tag already exists!";}
            pStatement = null;
            pStatement = connection.prepareStatement("insert into [" + guild.getId() + "] (name,tag,user,userid,date) " +
                    "values (?,?,?,?," + new java.util.Date(System.currentTimeMillis() - 3600 * 1000).getTime() + ")");
            pStatement.setString(1, name);
            pStatement.setString(2, tag);
            pStatement.setString(3, user);
            pStatement.setString(4, userID);
            pStatement.executeUpdate();
            SimpleLog.getLog("SQLite").info("Created a tag: " + name);
            returnString = "Tag " + name + " created!";
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            SimpleLog.getLog("SQLite").warn("Failure writing a tag!");
        } finally {
            closeConnection();
        }
        return returnString;
    }

    String tagd(String tag) {
        String returnString = "Error occured in tag deletion!";
        try {
            openConnection("tags");
            pStatement = connection.prepareStatement("delete from [" + guild.getId() + "] where name == ?");
            pStatement.setString(1,tag);
            pStatement.executeUpdate();
            returnString = "Tag " + tag + " successfully deleted!";
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            SimpleLog.getLog("SQLite").warn("Failure deleting tag " + tag + "!");
        } finally {
            closeConnection();
        }
        return returnString;
    }

    String tag(String tag) {
        String returnString = "Something went wrong getting this tag!";
        try {
            String tagFinal;
            openConnection("tags");
            pStatement = connection.prepareStatement("select tag from [" + guild.getId() + "] where name = (?)");
            pStatement.setString(1,tag);
            ResultSet rs = pStatement.executeQuery();
            if(!rs.next()) {returnString = "No tag found by this name!";}
            else if (rs.getFetchSize() != 0) { returnString = "Multiple tags found by this name!";}
            else {do{tagFinal = rs.getString("tag");}while(rs.next()); returnString = tagFinal;}
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            SimpleLog.getLog("SQLite").warn("Error occurred fetching tag!");
        } finally {
            closeConnection();
        }
        return returnString;
    }

    MessageEmbed tagl() {
        EmbedBuilder embedBuilder = new EmbedBuilder().setTitle("List of tags in " + guild.getName(), null);
        try {
            openConnection("tags");
            ResultSet rs = statement.executeQuery("select name from [" + guild.getId() + "]");
            if(!rs.next()) {closeConnection(); embedBuilder.setDescription("No tags in this server!");}
            else {
                do {
                    embedBuilder.addField(rs.getString("name"), "", true);
                }while (rs.next());
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            SimpleLog.getLog("SQLite").warn("Error fetching list of tags for this guild!");
        } finally {
            closeConnection();
        }
        return embedBuilder.build();
    }

    MessageEmbed tagi(String tag) {
        EmbedBuilder returnMsg = new EmbedBuilder().setTitle("Information on tag: " + tag, null);
        try {
            openConnection("tags");
            pStatement = connection.prepareStatement("select * from [" + guild.getId() + "] where name = (?)");
            pStatement.setString(1,tag);
            ResultSet rs = pStatement.executeQuery();
            if(!rs.next()) {returnMsg.appendDescription("No such tag found!");}
            else if (rs.getFetchSize() != 0) {returnMsg.appendDescription("Multiple tags found by this name!");}
            else {
                do {
                    returnMsg
                            .addField("Creator", rs.getString("user"), true)
                            .addField("Creator ID", rs.getString("userid"), true)
                            .addField("Created On", String.valueOf(new java.util.Date(rs.getLong("date"))),false);
                }while(rs.next());
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            SimpleLog.getLog("SQLite").warn("Error occured fetching tag information!");
            returnMsg.appendDescription("Something went wrong fetching information for this tag!");
        } finally {
            closeConnection();
        }
        return returnMsg.build();
    }

    void enableTracking(String voiceID) {
        try {
            openConnection("DVRTrack");
            if(statement.executeQuery("select * from [" + guild.getId() + "] where id = " + voiceID).next()) {
                pStatement = connection.prepareStatement("select tracking from [" + guild.getId() + "] where id = " + voiceID);
                if (!pStatement.executeQuery().getBoolean("tracking")) {
                    statement.executeUpdate("update [" + guild.getId() + "] " + "set tracking = " + 1
                            +" where id = " + voiceID);
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            SimpleLog.getLog("Dynamic Voice Rooms").warn("Failed to enable tracking on room!");
        } finally {
            closeConnection();
        }
    }

    List<String> listTrackedRooms() {
        List<String> returnList = new ArrayList<>();
        try {
            openConnection("DVRTrack");
            ResultSet rs = statement.executeQuery("select id from [" + guild.getId() + "] where tracking = " + 1);
            if(!rs.isClosed()) {
                do {
                    returnList.add(rs.getString(1));
                }while (rs.next());
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            SimpleLog.getLog("Dynamic Voice Rooms").warn("Failed to retrieve list of room ids!");
        } finally {
            closeConnection();
        }

        return  returnList;
    }

    boolean newRoom(String voiceID, String userID) {
        boolean returnValue;

        try {
            openConnection("DVRTrack");
            if(statement.executeQuery("select * from [" + guild.getId() + "] where userID = " + userID).isClosed()) {
                statement.executeUpdate("insert into [" + guild.getId() + "] (id, userid, tracking) " +
                        "values ("+ voiceID +","+ userID +"," + 0 + ")");
                returnValue = true;
            } else {returnValue = false;}
        } catch (Exception e) {
            e.printStackTrace();
            SimpleLog.getLog("Dynamic Voice Rooms").warn("Failed to store created room!");
            returnValue = false;
        } finally {
            closeConnection();
        }

        return returnValue;
    }

    void deleteRoom(String voiceID) {
        try {
            openConnection("DVRTrack");
            statement.executeUpdate("delete from [" + guild.getId() + "] where id = " + voiceID);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            SimpleLog.getLog("Dynamic Voice Rooms").warn("Failed to delete record on voice room!");
        } finally {
            closeConnection();
        }
    }

    String isDVROwner(String userID) {
        String returnValue;

        try {
            openConnection("DVRTrack");
            ResultSet rs = statement.executeQuery("select id from [" + guild.getId() + "] where userid = " + userID);
            if(!rs.isClosed()) {returnValue = rs.getString(1);}
            else {returnValue = null;}
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            SimpleLog.getLog("Dynamic Voice Rooms").warn("Unable to fetch voice room paired with userID!");
            returnValue = null;
        } finally {
            closeConnection();
        }

        return returnValue;
    }

    /*
        Connection managers.
     */
    private void openConnection(String db) throws ClassNotFoundException, SQLException {
        String dir = "." + File.separator + "databases" + File.separator;
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:" + dir + db + ".db");
        statement = connection.createStatement();
        statement.setQueryTimeout(30);
    }

    private void closeConnection() {
        try {
            if(!connection.isClosed()) {connection.close();}
        } catch (SQLException e) {
            e.printStackTrace();
        }
        statement = null;
        //connection = null;
        pStatement = null;
    }
}
