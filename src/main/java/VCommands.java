import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.managers.GuildController;

/**
 * Created by Xavier on 1/16/2017.
 */
class VCommands {

    /*
        Dynamic Voice Rooms system below
     */

    String room(String name, String userID, GuildController controller) {
        Channel channel = controller.createVoiceChannel(name).complete();
        if(new SQLiteCore(controller.getGuild(), false).newRoom(channel.getId(), userID)) {
            return "New voice channel created! Consult the help command to learn how to change information on the channel!";
        } else {
            channel.delete().queue();
            return "Either an error occured, or you already have a room created! If this is in error and persists," +
                    " use the feedback command please!";
        }
    }

    String setDVRName(String name, String userID, GuildController controller) {
        String returnValue = new SQLiteCore(controller.getGuild(), false).isDVROwner(userID);
        if(!returnValue.isEmpty()) {
            controller.getGuild().getVoiceChannelById(returnValue)
                    .getManager().setName(name).queue();
            returnValue = "Changed voice room name successfully!";
        } else {returnValue = "Either an error occurred, or you have not created a voice channel!";}

        return  returnValue;
    }

    String setUserLimit(int limit, String userID, GuildController controller) {
        String returnValue = new SQLiteCore(controller.getGuild(), false).isDVROwner(userID);
        if(!returnValue.isEmpty()) {
            controller.getGuild().getVoiceChannelById(returnValue)
                    .getManager().setUserLimit(limit).queue();
            returnValue = "Changed user limit successfully!";
        } else {returnValue = "Either an error occured, or you have not created a voice channel!";}
        return returnValue;
    }
}
