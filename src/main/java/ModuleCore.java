import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.utils.SimpleLog;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xavier on 4/19/2017.
 *
 * This class is the core of Xonah's module system. Handles writing to and retrieving data from said JSON file.
 *
 * This class will be used in 2 different ways depending on what variable is passed into the constructor.
 *
 * If a list of guilds is passed into the constructor, this will allow for running the initialization functions.
 *
 * If a guild object is passed into the constructor only the ability to modify guild specific variables
 * will be available.
 */

public class ModuleCore {
    private boolean isInit = false;
    private JSONObject mainObj = null;
    private JSONObject guildObj = null;
    private JSONArray moduleObj = null;
    private JSONArray mastersObj = null;
    private List<String> modules = new ArrayList<>();

    private List<Guild> guilds = null;
    private Guild guild = null;

    //Used for initialization, error checking.
    ModuleCore(List<Guild> guilds) throws IOException, ParseException {
        Object obj = new JSONParser().parse(new FileReader("guildconfig.json"));
        mainObj = (JSONObject) obj;
        isInit = true;
        this.guilds = guilds;
    }

    //Working with guild specific functions
    ModuleCore(Guild guild, boolean guildChange) {
        try {
            this.guild = guild;
            mainObj = (JSONObject) new JSONParser().parse(new FileReader("guildconfig.json"));
            if(guildChange) {
                if(mainObj.containsKey(guild.getId())) {
                    deleteGuild();
                } else {
                    newGuild();
                }
            } else {
                guildObj = (JSONObject) mainObj.get(guild.getId());
                mastersObj = (JSONArray) guildObj.get("botmasters");
                moduleObj = (JSONArray) guildObj.get("modules");
                modules = moduleObj.subList(0, moduleObj.size());
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    //Function called to save changes to JSON file.
    private void write() {
        try {
            FileWriter fileWriter = new FileWriter(new File("guildconfig.json"));
            mainObj.put(guild.getId(), guildObj);
            fileWriter.write(mainObj.toJSONString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Check the data inside of the JSON file.
    void init() throws IOException {
        List<Guild> resetGuilds = new ArrayList<>();
        if(isInit) {
            for(int i=0;i<guilds.size();i++) {
                guild = guilds.get(i);
                if (!mainObj.containsKey(guilds.get(i).getId())) {
                    SimpleLog.getLog("Module Core").warn("Guild " + guild.getName() + " is missing their " +
                            "JSON entry! Setting reset flag and regenerating...");
                    guildObj = new JSONObject();
                    moduleObj = new JSONArray();

                    guildObj.put("modules", moduleObj);
                    guildObj.put("botmasters", new JSONArray());
                    guildObj.put("reset", true);
                    write();
                } else {
                    guildObj = (JSONObject) mainObj.get(guild.getId());

                    if (guildObj.containsKey("modules")) {
                        moduleObj = (JSONArray) guildObj.get("modules");

                        for (int i2 = 0; i2 < modules.size(); i2++) {
                            if (!mainObj.containsKey(modules.get(i2))) {
                                SimpleLog.getLog("Module Core").warn("JSON config for " + guild.getName() +
                                        "is partially corrupted. Setting reset flag and regenerating...");
                                guildObj.put("reset", true);
                            }
                        }
                        guildObj.put("modules", moduleObj);
                        write();
                    } else {
                        SimpleLog.getLog("Module Core").warn("JSON config for " + guild.getName() +
                                "is partially corrupted. Setting reset flag and regenerating...");
                        moduleObj = new JSONArray();
                        guildObj.put("modules", moduleObj);
                        write();
                    }

                    if (!guildObj.containsKey("botmasters")) {
                        SimpleLog.getLog("Module Core").warn("JSON config for " + guild.getName() +
                                "is partially corrupted. Setting reset flag and regenerating...");
                        guildObj.put("reset", true);
                        guildObj.put("botmasters", new JSONArray());
                        write();
                    }
                }
            }
            guildObj = (JSONObject) mainObj.get(guild.getId());
            boolean reset = (boolean) guildObj.get("reset");
            if(reset) {resetGuilds.add(guild);}
            notifyGuild(resetGuilds);
            //new FileWriter(new File("guildconfig.json")).write(mainObj.toJSONString());
        } else {
            SimpleLog.getLog("Module Core").warn("Calling init function in non-init mode!");
        }
    }

    private void newGuild() throws IOException {
        guildObj = new JSONObject();
        guildObj.put("botmasters", new JSONArray());
        guildObj.put("reset", false);
        guildObj.put("modules", new JSONArray());
        write();
    }

    private void deleteGuild() throws IOException {
        FileWriter fileWriter = new FileWriter(new File("guildconfig.json"));
        mainObj.remove(guild.getId());
        fileWriter.write(mainObj.toJSONString());
        fileWriter.close();
    }

    private void notifyGuild(List<Guild> resetGuilds) {
        for(int i=0;i<resetGuilds.size();i++) {
            resetGuilds.get(i).getOwner().getUser().openPrivateChannel().complete()
                    .sendMessage("Error detected in your guild's module configuration! \n " +
                            "It's recommended you check your settings!").queue();
            guildObj = (JSONObject) mainObj.get(resetGuilds.get(i).getId());
            guildObj.put("reset", false);
            write();
        }
    }

    List<String> getModules() {
        return modules;
    }

    void toggleModule(String module) {
        if(modules.contains(module)) {
            modules.remove(module);
            moduleObj.remove(module);
        } else {
            modules.add(module);
        }
        guildObj.put("modules", moduleObj);
        write();
    }

    List<String> getBotmasters() {
        return mastersObj.subList(0,mastersObj.size());
    }

    void setBotmaster(String masterID) {
        if(mastersObj.contains(masterID)) {
            mastersObj.remove(masterID);
        } else {
            mastersObj.add(masterID);
        }
        guildObj.put("botmasters", mastersObj);
        write();
    }

    List<String> getRoles() {
        JSONArray roleObj = (JSONArray) guildObj.get("roles");
        List<String> returnLst = new ArrayList<>();

        if(!roleObj.isEmpty()) {returnLst = roleObj.subList(0, roleObj.size());}

        return returnLst;
    }

    void setRole(String roleID) {
        if(!guildObj.containsKey("roles")) {guildObj.put("roles", new JSONArray()); write();}
        JSONArray roleObj = (JSONArray) guildObj.get("roles");
        if(roleObj.contains(roleID)) {
            roleObj.remove(roleID);
        } else {
            roleObj.add(roleID);
        }
        write();
    }

    void setRChannel(String channel) {
        JSONArray rChannelObj = new JSONArray();
        if(!guildObj.containsKey("rChannel")) {
            rChannelObj.add(channel);
            guildObj.put("rChannel", rChannelObj);
        } else {
            rChannelObj = (JSONArray) guildObj.get("rChannel");
            guildObj.put("rChannel", rChannelObj.add(channel));
        }
        write();
    }

    String getRChannelID() {
        String returnStr;
        if(!guildObj.containsKey("rChannel")) {returnStr = "ERROR";} else {
            JSONArray rChannel = (JSONArray) guildObj.get("rChannel");
            returnStr = (String) rChannel.get(0);
        }
        return returnStr;
    }

    void setLChannel(String channel) {
        JSONArray rChannelObj = new JSONArray();
        if(!guildObj.containsKey("lChannel")) {
            rChannelObj.add(channel);
            guildObj.put("lChannel", rChannelObj);
        } else {
            rChannelObj = (JSONArray) guildObj.get("rChannel");
            guildObj.put("rChannel", rChannelObj.add(channel));
        }
        write();
    }

    String getLChannelID() {
        String returnStr;
        if(!guildObj.containsKey("lChannel")) {returnStr = "ERROR";} else {
            JSONArray rChannel = (JSONArray) guildObj.get("lChannel");
            returnStr = (String) rChannel.get(0);
        }
        return returnStr;
    }
}